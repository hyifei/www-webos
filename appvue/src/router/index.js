import Vue from 'vue'
import Router from 'vue-router'
import device from '@/views/Home.vue'
import devicelist from '@/views/device/list.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/device/home'
    },
    {
      path: '/device/home',
      component: device,
      name: 'device',
      children:[
        {
          path: 'device/list',
          component: devicelist,
          name: 'devicelist'
        }
      ]
    }
  ]
})
