/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import tablelist from '../components/tablelist'
import global from "../components/global"
import table from "../components/table"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/tablelist'
    }, {
      path: '/tablelist',
      component: tablelist
    },
    {
      path: '/global',
      component: global
    },
    {
      path: '/table',
      component: table
    },

  ]
})
