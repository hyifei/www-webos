// 导入组件
import Vue from 'vue';
import Router from 'vue-router';
// 登录
import login from '@/views/login';
// 首页
import index from '@/views/index';
/**
 * 基础菜单
 */

import gatewayInfo from '@/views/user/gatewayInfo';
import china from '@/views/user/china';
import province from '@/views/user/province';
import dialog from '@/views/user/dialog';
// 启用路由
Vue.use(Router);

// 导出路由 
export default new Router({
    routes: [{
        path: '/',
        redirect: '/user/china',
    }, {
        path: '/login',
        name: '登录',
        component: login,
        hidden: true,
        meta: {
            requireAuth: false
        }
    },
	{
		path: '/user/gatewayInfo',
		name: '网关详情页',
		component: gatewayInfo,
		hidden: true,
		meta: {
		    requireAuth: false
		}
	},
	{
		path: '/user/china',
		name: '全国地图',
		component: china,
		hidden: true,
		meta: {
		    requireAuth: false
		}
	},
	{
		path: '/user/province',
		name: '省份地图',
		component: province,
		hidden: true,
		meta: {
		    requireAuth: false
		}
	},
	{
		path: '/user/dialog',
		name: '省份地图',
		component: dialog,
		hidden: true,
		meta: {
		    requireAuth: false
		}
	},
	]
})